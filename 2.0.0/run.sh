#!/usr/bin/env bash

$ROOT=${OPENHAB_CONF:-/opt/openhab2}

docker run
  --detach \
  --net host \
  --restart always \
  --name openhab2 \
  --volume /etc/localtime:/etc/localtime:ro \
  --volume /etc/timezone:/etc/timezone:ro \
  --volume "$ROOT/conf:/openhab/conf" \
  --volume "$ROOT/userdata:/openhab/userdata" \
  --volume "$ROOT/addons:/openhab/addons" \
  --env USER_ID=$(id -u openhab) \
  openhab/openhab:2.0.0-amd64
