#!/usr/bin/env bash

$ROOT=${OPENHAB_CONF:-/opt/openhab}

docker run 
  --detach \
  --net host \
  --restart always \
  --name openhab \
  --volume /etc/localtime:/etc/localtime:ro \
  --volume /etc/timezone:/etc/timezone:ro   \
  --volume $ROOT/configurations:/openhab/configurations \
  --volume $ROOT/userdata:/openhab/userdata \
  --volume $ROOT/addons:/openhab/addons \
  --env USER_ID=$(id -u openhab) \
  openhab/openhab:1.8.3-amd64
